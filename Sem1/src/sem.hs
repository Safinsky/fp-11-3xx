data MyTerm = Variable String
          | Lambda String MyTerm
          | Apply MyTerm MyTerm

instance Show MyTerm where
    show (Variable x) = x
    show (Lambda variable term) = "(\\" ++ variable ++ "." ++ (show term) ++ ")" 
    show (Apply t1 t2) = (show t1) ++ " " ++ (show t2)
	
evalOneStep :: MyTerm -> MyTerm
evalOneStep (Apply (Lambda variable term) t) = setVariable variable [] term t
evalOneStep (Apply (Apply term1 term2) t) = Apply (evalOneStep (Apply term1 term2)) t
evalOneStep term = term

evalFullSteps :: MyTerm -> MyTerm
evalFullSteps (Apply (Lambda variable term) t) = evalFullSteps (setVariable variable [] term t)
evalFullSteps (Apply (Apply term1 term2) t) = evalFullSteps (Apply (evalFullSteps (Apply term1 term2)) t)
evalFullSteps term = term


setVariable :: String -> [String] -> MyTerm -> MyTerm -> MyTerm
setVariable var list (Variable x) newTerm = if x == var then freeVarRename list newTerm
                                            else Variable x    
setVariable var list (Lambda x t) newTerm = if x /= var then Lambda x (setVariable var (x:list) t newTerm)
                                            else Lambda x t											  
setVariable var list (Apply t1 t2) newTerm = Apply (setVariable var list t1 newTerm) (setVariable var list t2 newTerm)
 									
															
freeVarRename :: [String] -> MyTerm -> MyTerm
freeVarRename list (Variable x) = if elem x list then Variable (rename list x 0)
                                  else Variable x									  
freeVarRename list (Lambda x term) = if elem x list then Lambda x (freeVarRename (filter (\el -> el /= x) list) term)
                                     else Lambda x (freeVarRename list term)									 
freeVarRename list (Apply t1 t2) = Apply (freeVarRename list t1) (freeVarRename list t2)
								  									  
									  
rename :: [String] -> String -> Int -> String
rename list x n = if elem (x ++ (show n)) list then rename list x (n+1)
                  else x ++ (show n)






