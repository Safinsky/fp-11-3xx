isOddComp :: Integer -> Bool
isOddComp p | mod p 2 == 0 = False
            | isPrime p  = False
            | otherwise = True
			
isPrime :: Integer -> Bool
isPrime p | p <= 0 = False
          | p == 2 = True
          | mod p 2 == 0 = False
          | otherwise = checkPrimeToX p 3

checkPrimeToX :: Integer -> Integer -> Bool		  
checkPrimeToX p x | p < x * x = True
                  | mod p x == 0 = False
                  | otherwise = checkPrimeToX p (x + 2)	
		     
mainOddComp :: Integer -> Integer			 
mainOddComp a | isOddComp a = mainPrime a (a - 2)
              | otherwise = mainOddComp (a + 1) 
			  
mainPrime :: Integer -> Integer -> Integer		  
mainPrime a b | b == 1 = myShow a
              | isPrime b = result a b
              | otherwise = mainPrime a (b - 1)
				
myShow :: Integer -> Integer	
myShow a = a

result :: Integer -> Integer -> Integer
result a b | (a - b) `mod` 2 == 0 && round (count a b) `mod` 100 >= 5 && round (count a b) - 1 `mod` 100 == 0 = mainOddComp (a + 1)
           | (a - b) `mod` 2 == 0 && round (count a b) `mod` 100 < 5 && round (count a b) `mod` 100 == 0 = mainOddComp (a + 1)
           | otherwise = mainPrime a (b - 1)

count :: Integer -> Integer -> Double
count curNumber prNumber = sqrt (fromIntegral(curNumber - prNumber) / 2) * 100
						 